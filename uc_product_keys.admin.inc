<?php

/**
 * @file
 * This file handles code used typically by the administrator.
 */

/*******************************************************************************
 * Callback Functions, Forms, and Tables
 ******************************************************************************/

////////////////////////////////////////
// LIST/ADD/EDIT SEQUENCES
////////////////////////////////////////

/**
 * This form lets you view and edit the product sequence keys. To
 * edit, you need to have 'administer product keys' access.
 *
 * @return The sequences listing form.
 */
function uc_product_keys_list_sequences_form($form, &$form_state) {
  $admin = uc_product_keys_can_administer_keys();
  $form = array();

  // Now start on creating the table of existing sequences.
  // Begin by getting all the sequences

  $sequences = uc_product_keys_db_get_all_sequences();

  // Save the sequences in the form so that other methods may have
  // access to them

  $form['sequences'] = array(
    '#type' => 'value',
    '#value' => $sequences,
  );

  // The idea is to generate all the form values here and then add
  // everything else when we theme the form. If there are no sequences
  // or if you don't have 'administer product keys' access, there
  // won't be any form items.

  if ($sequences && $admin) {
    foreach ($sequences as $sequence) {

      // NOTE: Any hook_form_alter() methods that want to add form
      // elements to a sequence row should name it 'some_name_' .
      // $sequence->pksid. Later on, we will use this pattern to find
      // all rows with elements that have changed, so that we only
      // update those rows

      // The three fields we can edit are the name, the dynamic flag
      // and the encryption flag

      $form['name_' . $sequence->pksid] = array(
        '#type' => 'textfield',
        '#size' => 20,
        '#default_value' => check_plain($sequence->name),
      );
      $form['dynamic_' . $sequence->pksid] = array(
        '#type' => 'checkbox',
        '#default_value' => $sequence->dynamic,
      );
      $form['encrypt_' . $sequence->pksid] = array(
        '#type' => 'checkbox',
        '#default_value' => $sequence->encrypt,
      );

      // Also allow for deletions

      $form['delete_' . $sequence->pksid] = array(
        '#type' => 'checkbox',
        '#default_value' => FALSE,
      );
    }

    // Add a submit buttom

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update sequences'),
      '#submit' => array('uc_product_keys_list_sequences_form_submit'),
    );
  }

  // Allow the user to add a new sequence
  // But only if he has permission

  if ($admin) {

    // Save a sequence object that the submit functions call fill with
    // values. By adding this now, other modules have a way to add to
    // the sequence

    $sequence = new stdClass();

    $form['uc_product_keys_add_sequence'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add a product key sequence'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['uc_product_keys_add_sequence']['sequence'] = array(
      '#type' => 'value',
      '#value' => $sequence,
    );

    $form['uc_product_keys_add_sequence']['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Product key sequence name'),
      '#description' => t('Select a unique name for this sequence.'),
      '#maxlength' => 45,
    );
    $form['uc_product_keys_add_sequence']['dynamic'] = array(
      '#type' => 'checkbox',
      '#title' => t('Dynamically generate product keys'),
      '#description' => t('You will need a module that implements hook_uc_product_keys_api().'),
      '#default_value' => FALSE,
    );
    $form['uc_product_keys_add_sequence']['encrypt'] = array(
      '#type' => 'checkbox',
      '#title' => t('Encrypt the keys in the database'),
      '#description' => t('You will need a module that implements hook_uc_product_keys_decrypt().'),
      '#default_value' => FALSE,
    );

    $form['uc_product_keys_add_sequence']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add sequence'),
      '#submit' => array('uc_product_keys_add_sequence_form_submit'),
    );
  }

  return $form;
}

/**
 * The list sequences form has been submitted. Update the database for
 * any changes.
 *
 * @param $form The form.
 * @param $form_state The form state.
 */
function uc_product_keys_list_sequences_form_submit($form, &$form_state) {
  // Get the sequences from where we cached them

  $sequences = $form['sequences']['#value'];

  // Update the sequences as needed. Also, collect any sequences that
  // need to be deleted

  $deletions = array();

  $count = 0;
  $errors = array();
  foreach ($sequences as $sequence) {

    // If we need to delete something, store it for later
    // No need to update it

    if ($form['delete_' . $sequence->pksid]['#value']) {
      $deletions[] = $sequence->pksid;
    }
    else {
      $sequence->name = $form['name_' . $sequence->pksid]['#value'];
      $sequence->dynamic = $form['dynamic_' . $sequence->pksid]['#value'];
      $sequence->encrypt = $form['encrypt_' . $sequence->pksid]['#value'];

      // Check for any changes
      // We inspect each form item with a default value to see if the
      // value has changed

      $dirty = FALSE;

      $end_string = '_' . $sequence->pksid;
      $end_length = strlen($end_string);

      foreach ($form as $name => $item) {
        if ((strrpos($name, $end_string) == strlen($name) - $end_length) && $item['#default_value'] != $item['#value']) {
          $dirty = TRUE;
          break;
        }
      }

      if ($dirty == TRUE) {
        if (uc_product_keys_db_update_sequence($sequence)) {
          $count++;
        }
        else {
          $errors[] = $sequence->name;
        }
      }
    }
  }

  // Check for errors

  if ($errors) {
    foreach ($errors as $error) {
      drupal_set_message(t('Failed to update sequence "@seq".', array("@seq" => $error), array('langcode' => 'error')));
    }
  }
  else {
    if ($count == 0) {
      if (!$deletions) {
        drupal_set_message(t('No changes were made to the product key sequences.'));
      }
    }
    else {
      drupal_set_message(t('Updated !seq.', array('!seq' => format_plural($count, '1 sequence', "@count sequences"))));
    }

    // Do we need to delete anything? (Only if no errors)

    if ($deletions) {
      drupal_goto('admin/store/products/keys/sequences/delete', array('query' => array('d' => $deletions)));
    }
  }
}

/**
 * This handles the 'Add sequence' button on the sequence list form.
 * It adds a sequence to the list.
 *
 * @parm $form The form.
 * @param $form_state The form state.
 */
function uc_product_keys_add_sequence_form_submit($form, &$form_state) {
  $sequence = $form['uc_product_keys_add_sequence']['sequence']['#value'];

  $sequence->pksid = 0;
  $sequence->name = $form['uc_product_keys_add_sequence']['name']['#value'];
  $sequence->dynamic = $form['uc_product_keys_add_sequence']['dynamic']['#value'];
  $sequence->encrypt = $form['uc_product_keys_add_sequence']['encrypt']['#value'];

  if (!uc_product_keys_db_insert_sequence($sequence)) {
    drupal_set_message(t('Failed to insert the sequence into the database.'), 'error');
  }
  $form_state['new_sequence_id'] = $sequence->pksid;
}

// Helper functions for sorting the sequences table

function uc_product_keys_sort_desc($a, $b) {
  preg_match('/value="([^"]*)"/', $a['data'][0]['data'], $matches);
  $a = $matches[1];
  preg_match('/value="([^"]*)"/', $b['data'][0]['data'], $matches);
  $b = $matches[1];
  return strcmp($a, $b);
}

function uc_product_keys_sort_asc($a, $b) {
  return uc_product_keys_sort_desc($b, $a);
}

/**
 * We now take out very rough form and theme it into something useful.
 *
 * If you have 'administer product keys' access, the theming begins by
 * adding the add sequence form, so that we can add new sequences to
 * the list.
 *
 * Next, we list all the sequences in a table. If you have 'administer
 * product keys' access, the sequences fields are editable; if not,
 * they are just labels.
 *
 * @param form The form to them.
 * @return The themed form.
 */
function theme_uc_product_keys_list_sequences_form($variables) {
  $form = $variables['form'];
  // Get the sequences from where we cached them

  $sequences = $form['sequences']['#value'];

  $admin = uc_product_keys_can_administer_keys();

  $output = '';

  // If there are any sequences, list them in a sortable table

  if ($sequences) {
    $header = array();
    $header[] = array(
      'data' => t('Sequence name'),
      'field' => 'name',
      'sort' => 'desc',
    );
    $header[] = array('data' => t('Dyn'));
    $header[] = array('data' => t('Enc'));
    uc_product_keys_theme_list_api('sequence_header', $header);
    if ($admin) {
      $header[] = array('data' => t('Del?'));
    }

    $rows = array();
    foreach ($sequences as $sequence) {
      $row = array();

      // Editable version

      if ($admin) {
        $row[] = array('data' => drupal_render($form['name_' . $sequence->pksid]));
        $row[] = array('data' => drupal_render($form['dynamic_' . $sequence->pksid]));
        $row[] = array('data' => drupal_render($form['encrypt_' . $sequence->pksid]));
        uc_product_keys_theme_list_api('sequence_row', $row, $form, $sequence);
        $row[] = array('data' => drupal_render($form['delete_' . $sequence->pksid]));
      }

      // Non-editable version

      else {
        $row[] = array('data' => check_plain($sequence->name));
        $row[] = array('data' => $sequence->dynamic ? t('Yes') : t('No'));
        $row[] = array('data' => $sequence->encrypt ? t('Yes') : t('No'));
        uc_product_keys_theme_list_api('sequence_row', $row, $form, $sequence);
      }
      $rows[] = array('data' => $row);
    }

    // We have to do the sorting ourselves because the data is cached
    // in memory and not in the database

    $sort = tablesort_get_sort($header);
    usort($rows, 'uc_product_keys_sort_' . $sort);

    // Theme it all into a table

    $output .= theme('table', array('header' => $header, 'rows' => $rows));

  }

  // If there are no sequences, say so

  else {
    $output .= '<p>' . t('You have not defined any product key sequences.') . '</p>';
  }

  // One more call to drupal_render to get any remaining elements

  $output .= drupal_render_children($form);

  return $output;
}

////////////////////////////////////////
// DELETE SEQUENCES
////////////////////////////////////////

/**
 * Create the delete sequences form. This is used for confirming a
 * deletion, so it's just a submit button.
 *
 * @param form_state The form state.
 * @returns The form.
 */
function uc_product_keys_delete_sequences_form($form, &$form_state) {
  // The parameters are stuffed into the URL

  $tdelete = array();
  if (isset($_GET['d']) && is_array($_GET['d'])) {
    $tdelete = $_GET['d'];
  }

  // Validate the parameters

  $delete = array();
  foreach ($tdelete as $pksid) {
    $pksid = check_plain($pksid);
    $result = uc_product_keys_db_get_sequence($pksid);
    if ($result !== FALSE) {
      $delete[] = $pksid;
    }
  }
  $count = count($delete);

  // Cache the validated parameters

  $form['delete'] = array(
    '#type' => 'value',
    '#value' => $delete,
  );

  // Here's the one form element: the Delete button

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete !seq', array('!seq' => format_plural($count, 'sequence', '@count sequences'))),
    '#submit' => array('uc_product_keys_delete_sequences_form_submit'),
  );

  return $form;
}

/**
 * Delete the sequences that were selected from the sequence list.
 When done, return to the page that lists the sequences.
 *
 * @param $form The form.
 * @param $form_state The form state.
 */
function uc_product_keys_delete_sequences_form_submit($form, &$form_state) {
  $valid_deletions = 0;
  $delete = $form['delete']['#value'];

  foreach ($delete as $pksid) {
    $sequence = uc_product_keys_db_get_sequence($pksid);
    $result = uc_product_keys_db_delete_sequence($sequence);
    if ($result === FALSE) {
      drupal_set_message(t('Failed to delete sequence ID @pksid.', array('@pksid' => $pksid)), 'error');
    }
    else {
      $valid_deletions++;
    }
  }

  drupal_set_message(format_plural($valid_deletions, '1 sequence deleted.', '@count sequences deleted'));
  drupal_goto('admin/store/products/keys/sequences');
}

/**
 * Theme the delete sequences form. We display the names of the
 * sequences being deleted, warn the user about potential problems and
 * display the submit button.
 *
 * @param $form The form.
 * @returns The themed output.
 */
function theme_uc_product_keys_delete_sequences_form($variables) {
  $form = $variables['form'];
  $delete = $form['delete']['#value'];

  $output = '';
  if ($delete) {
    $output .= '<p>' . t('Delete the following sequences?') . '<ul>';
    foreach ($delete as $pksid) {
      $sequence = uc_product_keys_db_get_sequence($pksid);
      if ($sequence) {
        $output .= '<li>' . check_plain($sequence->name) . '</li>';
      }
    }
    $output .= '</ul><p>';
    $output .= t(
      'When a product key sequence is deleted, any products associated with that sequence will no '
      . 'longer have a source for product keys. Unless you make changes, customers can still buy the '
      . 'product, but will not receive a product key. Also, any unassigned keys in that sequence will '
      . 'be deleted. Assigned keys will remain, but their link to the sequence will be zeroed.');
    $output .= '</p><p>' . t('Deletion is permanent. There is no undo.') . '</p>';
    $output .= drupal_render_children($form);
    $output .= '&nbsp;&nbsp;' . l(t('Cancel'), 'admin/store/products/keys/sequences');
  }
  else {
    $output .= '<p>' . t('There are no sequences to delete.') . '</p>';
  }
  return $output;
}

////////////////////////////////////////
// LIST/EDIT PRODUCT KEYS
////////////////////////////////////////

/**
 * Create the list/edit keys support. People with 'administer product
 * keys' access will be allowed to edit, although there is not much
 * that is editable.
 *
 * Type $type variable can be set to display all product keys, or only
 * product keys associated with a product, user or sequence.
 *
 * @param $form_state The form state.
 * @param $type 'all', 'sequence', 'product' or 'user'
 * @param $arg If 'all', then NULL; if 'sequence', then the sequence
 * 	object, if 'product', then the product object; if 'user', then
 * 	the user object.
 * @return The form.
 */
function uc_product_keys_list_form($form, &$form_state, $type, $arg) {
  $admin = uc_product_keys_can_administer_keys();
  $view_all = uc_product_keys_can_view_all_keys();

  // To create a sortable table, we need to define the headers before
  // we get the data from the database

  $header = array();
  $long_header = array();
  if ($view_all) {
    $header[] = array(
      'data' => t('Key ID'),
      'field' => 'pkid',
      'sort' => 'asc',
    );
    $header[] = array(
      'data' => t('Sequence'),
      'field' => 'pksid',
    );
  }
  $header[] = array(
    'data' => t('Product'),
    'field' => 'title',
  );
  $header[] = array(
    'data' => t('Order ID'),
    'field' => 'order_id',
  );

  // We have one item that may require a lot of space -- the
  // product key itself. Also, check and see if there are any long
  // items that other modules want to contribute

  $long_header[] = array(
    'data' => t('Key'),
    'field' => 'product_key',
  );
  uc_product_keys_theme_list_api('key_long_header', $long_header);

  if ($admin) {
    $header[]  = array(
      'data' => t('# Act'),
      'field' => 'activations',
    );
    uc_product_keys_theme_list_api('key_header', $header);
    $header[] = array(
      'data' => t('Revoke?'),
      'field' => 'status',
    );
    $header[]  = array('data' => t('Del?'));
  }
  elseif ($view_all) {
    $header[] = array(
      'data' => t('Status'),
      'field' => 'status',
    );
    $header[]  = array(
      'data' => t('# Act'),
      'field' => 'activations',
    );
    uc_product_keys_theme_list_api('key_header', $header);
  }
  else {
    uc_product_keys_theme_list_api('key_header', $header);
  }

  // Get the keys

  $keys = uc_product_keys_db_get_keys($type, $arg, $header, 20);

  // Need to save some stuff for theming later

  $form['keys'] = array(
    '#type' => 'value',
    '#value' => $keys,
  );
  $form['header'] = array(
    '#type' => 'value',
    '#value' => $header,
  );
  $form['long_header'] = array(
    '#type' => 'value',
    '#value' => $long_header,
  );
  $form['type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );

  if ($keys) {
    $admin = uc_product_keys_can_administer_keys();
    foreach ($keys as $key) {
      if ($admin) {
        if ($key->status != UC_PRODUCT_KEYS_STATUS_UNASSIGNED) {
          $form['revoke_' . $key->pkid] = array(
            '#type' => 'checkbox',
            '#default_value' => $key->status == UC_PRODUCT_KEYS_STATUS_REVOKED ? 1 : 0,
          );
        }
        else {
          $form['delete_' . $key->pkid] = array(
            '#type' => 'checkbox',
            '#default_value' => FALSE,
          );
        }
      }
    }

    // Update the revokation state of the keys

    if ($admin) {
      $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Update keys'),
        '#submit' => array('uc_product_keys_list_form_submit'),
      );
    }
  }

  else {
    // Nothing to show
  }

  return $form;
}

/**
 * Handle the form submit for updating a user's keys.
 *
 * @param $form The form.
 * @param $form_state The form state.
 * @return The path where we should wind up.
 */
function uc_product_keys_list_form_submit($form, &$form_state) {
  $keys = $form['keys']['#value'];

  $deletions = array();

  foreach ($keys as $key) {
    $new_status = -1;
    if ($key->status == UC_PRODUCT_KEYS_STATUS_REVOKED && $form['revoke_' . $key->pkid]['#value'] == 0) {
      $new_status = UC_PRODUCT_KEYS_STATUS_ASSIGNED;
    }
    elseif ($key->status == UC_PRODUCT_KEYS_STATUS_ASSIGNED && $form['revoke_' . $key->pkid]['#value'] != 0) {
      $new_status = UC_PRODUCT_KEYS_STATUS_REVOKED;
    }
    if ($new_status > -1) {
      $key->status = $new_status;
      if (!uc_product_keys_db_update_key($key)) {
        drupal_set_message(t('Failed to update key "@key".', array("@key" => $key->pkid), array('langcode' => 'error')));
      }
    }

    if ($form['delete_' . $key->pkid]['#value']) {
      $deletions[] = $key->pkid;
    }
  }

  // Do we need to delete anything?

  if ($deletions) {
    drupal_goto('admin/store/products/keys/delete', array('query' => array('d' => $deletions, 'ret' => implode('/', arg()))));
  }
}

/**
 * Theme the view/edit key form.
 *
 * @param $form The form array to theme.
 * @return The themed form (as a string).
 * @ingroup themeable
 */
function theme_uc_product_keys_list_form($variables) {
  $form = $variables['form'];
  $keys = $form['keys']['#value'];
  $header = $form['header']['#value'];
  $long_header = $form['long_header']['#value'];
  $type = $form['type']['#value'];

  $admin = uc_product_keys_can_administer_keys();
  $view_all = uc_product_keys_can_view_all_keys();

  $output = '';

  if ($keys) {
    $rows = array();
    foreach ($keys as $key) {
      $row = array();
      $long_row = array();

      $product = node_load($key->nid);
      if ($key->pksid) {
        $sequence = uc_product_keys_db_get_sequence($key->pksid);
      }
      if ($view_all) {
        $row[] = array('data' => $key->pkid);
        $row[] = array('data' => $key->pksid ? check_plain($sequence->name) : t('None'));
      }
      $row[] = array('data' => isset($product->title) ? l($product->title, 'node/' . $key->nid) : t('None'));
      $row[] = array('data' => $key->order_id ? l($key->order_id, 'admin/store/orders/' . $key->order_id) : t('None'));

      // We have one item that may require a lot of space -- the
      // product key itself. Also, check and see if there are any long
      // items that other modules want to contribute

      $long_row[] = array('data' => str_replace("\n", '<br />', check_plain($key->product_key)));
      uc_product_keys_theme_list_api('key_long_row', $long_row, $form, $key);

      if ($admin) {
        $row[] = array('data' => $key->activations);
        uc_product_keys_theme_list_api('key_row', $row, $form, $key);
        $row[] = array('data' => isset($form['revoke_' . $key->pkid]) ? drupal_render($form['revoke_' . $key->pkid]) : t('-'));
        $row[] = array('data' => isset($form['delete_' . $key->pkid]) ? drupal_render($form['delete_' . $key->pkid]) : t('-'));
      }
      elseif ($view_all) {
        $row[] = array(
          'data' => $key->status == UC_PRODUCT_KEYS_STATUS_REVOKED ? t('Revoked') : ($key->status == UC_PRODUCT_KEYS_STATUS_ASSIGNED ? t('OK') : ''),
        );
        $row[] = array('data' => $key->activations);
        uc_product_keys_theme_list_api('key_row', $row, $form, $key);
      }
      else {
        uc_product_keys_theme_list_api('key_row', $row, $form, $key);
      }

      $count = count($long_row);
      $short_items = count($row);
      for ($i = 0; $i < $count; $i++) {
        $row[] = array(
          'data' => $long_header[$i]['data'],
          'class' => 'long-row-header',
        );
        $row[] = array(
          'data' => $long_row[$i]['data'],
          'class' => 'long-row-data',
          'colspan' => $short_items - ($view_all ? 2 : 1),
        );
      }

      $rows[] = array('data' => $row);
    }

    $table = theme('table', array('header' => $header, 'rows' => $rows));

    // Reshape the table to accommodate putting long items in their
    // own rows, while maintaining the odd/even consistency

    if ($long_header) {
      $is_odd = TRUE;
      $new_table = '';

      $pos = 0;
      while (($pos1 = strpos($table, '<tr', $pos)) !== FALSE) {

        // Copy everything from where we last ended to the start of
        // the row

        $new_table .= substr($table, $pos, $pos1 - $pos);
        $pos = $pos1;

        // Look for the first long row element

        $pos1 = strpos($table, '<td class="long-row-header"', $pos);

        // Copy all that

        $new_table .= substr($table, $pos, $pos1 - $pos) . '</tr>';
        $pos = $pos1;

        // Find the end of the row

        $pos2 = strpos($table, '</tr>', $pos) + 5;

        // Process each pair of long header/row -- turn them into
        // independent rows

        while ($pos < $pos2) {

          // Find the end of the second td

          $pos1 = strpos($table, '</td>', $pos);
          if ($pos1 === FALSE || $pos1 >= $pos2) {
            break;
          }
          $pos1 = strpos($table, '</td>', $pos1 + 1) + 5;
          $new_table .= '<tr class="' . ($is_odd ? 'odd' : 'even') . ' long-row">' . ($view_all ? '<td></td>' : '') . substr($table, $pos, $pos1 - $pos) . '</tr>';
          $pos = $pos1;
        }

        // One row done; ignore any remaining code

        $pos = $pos2;
        $is_odd = !$is_odd;
      }

      // All rows done; copy over whatever's left

      $table = $new_table . substr($table, $pos);
    }

    $pager = theme('pager', array('tags' => NULL, 'element' => 0, 'parameters' => array(), 'quantity' => 10));
    $output = $pager . $table . $pager;

    $output .= drupal_render_children($form);
  }
  else {
    switch ($type) {
      case 'all':
        $output .= '<p>' . t('There are no product keys.') . '</p>';
        break;
      case 'sequence':
        $output .= '<p>' . t('This sequence has no product keys.') . '</p>';
        break;
      case 'product':
        $output .= '<p>' . t('This product has no product keys.') . '</p>';
        break;
      case 'user':
        $output .= '<p>' . t('You have no product keys.') . '</p>';
        break;
    }
  }
  return $output;
}

////////////////////////////////////////
// DELETE KEYS
////////////////////////////////////////

/**
 * Create the delete keys form. This is used for confirming a
 * deletion, so it's just a submit button.
 *
 * @param form_state The form state.
 * @returns The form.
 */
function uc_product_keys_delete_form($form, &$form_state) {
  // The parameters are stuffed into the URL

  $tdelete = array();
  if (isset($_GET['d']) && is_array($_GET['d'])) {
    $tdelete = $_GET['d'];
  }

  // Validate the parameters

  $delete = array();
  foreach ($tdelete as $pkid) {
    $pksid = check_plain($pkid);
    $result = uc_product_keys_db_get_key($pkid);
    if ($result !== FALSE) {
      $delete[] = $pksid;
    }
  }
  $count = count($delete);

  // Cache the validated parameters

  $form['delete'] = array(
    '#type' => 'value',
    '#value' => $delete,
  );

  // Here's the one form element: the Delete button

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete !seq', array('!seq' => format_plural($count, 'key', '@count keys'))),
    '#submit' => array('uc_product_keys_delete_form_submit'),
  );

  return $form;
}

/**
 * Delete the product keys that were selected from the product key
 list. When done, return to the page that lists the keys.
 *
 * @param $form The form.
 * @param $form_state The form state.
 */
function uc_product_keys_delete_form_submit($form, &$form_state) {
  $valid_deletions = 0;
  $delete = $form['delete']['#value'];

  foreach ($delete as $pkid) {
    $key = uc_product_keys_db_get_key($pkid);
    $result = uc_product_keys_db_delete_key($key);
    if ($result === FALSE) {
      drupal_set_message(t('Failed to delete key ID @pkid.', array('@pkid' => $pkid)), 'error');
    }
    else {
      $valid_deletions++;
    }
  }

  if (uc_product_keys_can_modify_stock() && $valid_deletions) {
    uc_product_keys_db_synchronize_all_stock();
  }

  drupal_set_message(format_plural($valid_deletions, '1 key deleted.', '@count keys deleted'));

  // Return back to where we started

  if (isset($_GET['ret'])) {
    drupal_goto($_GET['ret']);
  }
}

/**
 * Theme the delete keys form. We display the IDs of the keys being
 * deleted, warn the user about potential problems and display the
 * submit button.
 *
 * @param $form The form.
 * @returns The themed output.
 */
function theme_uc_product_keys_delete_form($variables) {
  $form = $variables['form'];
  $delete = $form['delete']['#value'];

  $output = '';
  if ($delete) {
    $output .= '<p>' . t('Delete the following product keys?') . '<ul>';
    foreach ($delete as $pkid) {
      $output .= '<li>' . $pkid . '</li>';
    }
    $output .= '</ul><p>';
    $output .= t('Deletion is permanent. There is no undo.') . '</p>';
    $output .= drupal_render_children($form);

    if (isset($_GET['ret'])) {
      $path = $_GET['ret'];
      $output .= '&nbsp;&nbsp;' . l(t('Cancel'), $path);
    }
  }
  else {
    $output .= '<p>' . t('There are no product keys to delete.') . '</p>';
  }
  return $output;
}

////////////////////////////////////////
// IMPORT PRODUCT KEYS
////////////////////////////////////////

/**
 * Create the Import Key form.
 *
 * @return The form.
 */
function uc_product_keys_import_form($form, &$form_state) {
  $form['#attributes'] = array('enctype' => "multipart/form-data");
  $form['upload'] = array(
    '#type' => 'file',
    '#title' => t('File with product keys'),
    '#description' => t(
      'Import a file containing the product keys. You may use either of two formats.<br/>'
      . 'The first contains two values per line with the values separated by a tab. '
      . 'The first field is the sequence name of the sequence associated with the product key. '
      . 'The second field is the product key itself.<br/>'
      . 'The second format starts with the sequence '
      . 'name on one line followed by a product key on one or more lines. The end of the product '
      . 'key is signaled by a line with just the separator string listed below (the default is a period). '
      . 'This format allows for multi-line keys.'
      ),
  );

  $form['separator'] = array(
    '#type' => 'textfield',
    '#title' => t('Separator'),
    '#default_value' => check_plain(variable_get('uc_product_keys_key_separator', '.')),
    '#description' => t(
      'A line with only this text on it signals the end of one key and the start of the next. This '
      . 'is used only for files where the sequence name and product key are not all on one line.'
      ),
  );

  $form['uc_product_keys_sequence_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
  );

  return $form;
}

/**
 * Submit the Import Key form.
 *
 * @parm $form The form.
 * @param $form_state The form state.
 */
function uc_product_keys_import_form_submit($form, &$form_state) {
  variable_set('uc_product_keys_key_separator', trim($form['separator']['#value']));
  $file = file_save_upload('upload');
  if ($file == 0) {
    drupal_set_message(t('Your file failed to upload.'), 'warning');
  }
  else {
    $fd = fopen($file->uri, 'r');
    $line = fgets($fd);

    $valid = 0;
    $total = 0;
    $valid_by_seq = array();
    $errors = array(
      'Missing seq or key' => 0,
      'Seq or key blank' => 0,
      'Seq does not exist' => 0,
      'Seq is dynamic' => 0,
      'Db insert failed' => 0,
    );

    if (strpos($line, "\t") !== FALSE) {
      rewind($fd);
      while (!feof($fd)) {
        $line = trim(fgets($fd));
        if (!$line) {
          continue; // Ignore blank lines
        }
        $total++;

        // Get the sequence name and key

        $elements = explode("\t", $line, 2);
        if (count($elements) != 2) {
          $errors['Missing seq or key']++;
          continue;
        }
        $name = trim($elements[0]);
        $product_key = trim($elements[1]);

        // Check that both elements are present

        if (!$name || !$product_key) {
          $errors['Seq or key blank']++;
          continue;
        }
        $sequence = uc_product_keys_db_get_sequence_by_name($name);

        // Check that the sequence exists and is not dynamic

        if ($sequence === FALSE) {
          $errors['Seq does not exist']++;
          continue;
        }

        if ($sequence->dynamic) {
          $errors['Seq is dynamic']++;
          continue;
        }

        // Add the product key

        $product_key = (object) array(
          'pkid' => 0,
          'pksid' => $sequence->pksid,
          'uid' => 0,
          'order_id' => 0,
          'nid' => 0,
          'product_key' => $product_key,
          'activations' => 0,
          'status' => UC_PRODUCT_KEYS_STATUS_UNASSIGNED,
        );

        if (uc_product_keys_db_insert_key($product_key) !== FALSE) {
          $valid++;
          $valid_by_seq[$sequence->pksid] = $valid;
        }
        else {
          $errors['Db insert failed']++;
        }
      }
    }
    else {
      $separator = variable_get('uc_product_keys_key_separator', '.');
      $name = $product_key = '';
      rewind($fd);
      while (!feof($fd)) {
        $line = trim(fgets($fd));

        // End of a product key?

        if ($line == $separator) {
          $total++;

          // Check for missing parts

          if (!$name || !$product_key) {
            $errors['Seq or key blank']++;
            $name = $product_key = '';
            continue;
          }

          // Try to find the matching sequence

          $sequence = uc_product_keys_db_get_sequence_by_name($name);

          // Check that the sequence exists and is not dynamic

          if ($sequence === FALSE) {
            $errors['Seq does not exist']++;
            $name = $product_key = '';
            continue;
          }

          if ($sequence->dynamic) {
            $errors['Seq is dynamic']++;
            $name = $product_key = '';
            continue;
          }

          // Add the key

          $product_key = (object) array(
            'pkid' => 0,
            'pksid' => $sequence->pksid,
            'uid' => 0,
            'order_id' => 0,
            'nid' => 0,
            'product_key' => $product_key,
            'activations' => 0,
            'status' => UC_PRODUCT_KEYS_STATUS_UNASSIGNED,
          );
          if (uc_product_keys_db_insert_key($product_key) !== FALSE) {
            $valid++;
            $valid_by_seq[$sequence->pksid] = $valid;
          }
          else {
            $errors['Db insert failed']++;
          }

          // Reset for next

          $name = $product_key = '';
        }

        // If the name is blank, we're starting to get a new product key

        elseif ($name == '') {
          if (!$line) {
            continue; // Ignore blank lines
          }
          $name = $line;
        }

        // If the name is not blank, accumulate the lines until the
        // separator is reached

        else {
          if ($product_key) {
            $product_key .= "\n";
          }
          $product_key .= $line;
        }
      }
    }

    // Adjust stock levels, if needed

    if (uc_product_keys_can_modify_stock()) {
      foreach ($valid_by_seq as $pksid => $stock_level) {
        $nid = uc_product_keys_db_get_product_for_sequence($pksid);
        if ($nid && !$sequence->dynamic) {
          $product = node_load($nid);
          uc_stock_adjust($product->model, $stock_level, FALSE);
        }
      }
    }

    if ($total == 0) {
      drupal_set_message(t('No product keys found.'), 'warning');
    }
    elseif ($total != $valid) {
      drupal_set_message(t('Added !valid product keys out of !total read.', array('!valid' => $valid, '!total' => $total), 'warning'));
      if ($errors['Missing seq or key']) {
        drupal_set_message(t('Records with a missing sequence or key: !count', array('!count' => $errors['Missing seq or key'])), 'warning');
      }
      if ($errors['Seq or key blank']) {
        drupal_set_message(t('Records where the sequence or key was blank: !count', array('!count' => $errors['Seq or key blank'])), 'warning');
      }
      if ($errors['Seq does not exist']) {
        drupal_set_message(t('Records where the sequence did not exist: !count', array('!count' => $errors['Seq does not exist'])), 'warning'); //
      }
      if ($errors['Seq is dynamic']) {
        drupal_set_message(t('Records where the sequence was dynamic: !count', array('!count' => $errors['Seq is dynamic'])), 'warning');
      }
      if ($errors['Db insert failed']) {
        drupal_set_message(t('Records where the database insert failed: !count', array('!count' => $errors['Db insert failed'])), 'warning');
      }
    }
    else {
      drupal_set_message(format_plural($valid, '1 product key added', '@count product keys added'), 'status');
    }
  }
}

/**
 * Theme the Import Key form.
 *
 * @param $form The form.
 * @return The theme form (i.e. an HTML string).
 */
function theme_uc_product_keys_import_form($variables) {
  $form = $variables['form'];
  drupal_render_children($form);
}

////////////////////////////////////////
// STOCK
////////////////////////////////////////

/**
 * This form lets connect the product key and stock systems.
 *
 * @return The stock form.
 */
function uc_product_keys_stock_form($form, &$form_state) {
  $form['use_stock'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use the Ubercart stock system to track product keys?'),
    '#description' => t(
      '<p>The stock level for each product can be automatically set to match the number of product '
      . 'keys available for the product. This works correctly only if each product uses a different '
      . 'product key sequence.</p>'
      . '<p>If the stock levels are incorrect for any reason, you can fix them by using the '
      . 'Synchronize Stock Levels button below.</p>'
      . '<p>For dynamically generated keys, stock handling is automatically set to '
      . 'inactive.</p>'),
    '#default_value' => variable_get('uc_product_keys_use_stock', FALSE),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );

  $form['sync_stock'] = array(
    '#type' => 'submit',
    '#value' => t('Synchronize stock levels'),
    '#submit' => array('uc_product_keys_synchronize_stock'),
  );

  return $form;
}

/**
 * The stock form has been submitted. Update the database for
 * any changes.
 *
 * @param $form The form.
 * @param $form_state The form state.
 */
function uc_product_keys_stock_form_submit($form, &$form_state) {
  variable_set('uc_product_keys_use_stock', $form['use_stock']['#value']);
}

/**
 * Synchronize the stock levels and active/inactive state for all
 * sequences that are associated with products.
 *
 * @param $form The form.
 * @param $form_state The form state.
 */
function uc_product_keys_synchronize_stock($form, &$form_state) {
  if (variable_get('uc_product_keys_use_stock', FALSE)) {
    if (uc_product_keys_db_synchronize_all_stock()) {
      drupal_set_message(t('Stock levels have been successfully synchronized.'));
    }
    else {
      drupal_set_message(t('A database error occurred while trying to synchronize stock levels.'), 'error');
    }
  }
  else {
    drupal_set_message(t('Stock tracking is not enabled.'));
  }
}

/**
 * Theme the stock form.
 *
 * @param form The form to them.
 * @return The themed form.
 */
function theme_uc_product_keys_stock_form($variables) {
  $form = $variables['form'];
  return drupal_render_children($form);
}
