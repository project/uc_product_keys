<?php

/**
 * @file
 * Default rules configurations for the uc_product_keys module.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function uc_product_keys_default_rules_configuration() {
  $rule = rules_reaction_rule();
  $rule->label = t('E-mail product keys to a customer');
  $rule->active = TRUE;
  $rule->event('uc_product_keys_assigned')
    ->action('uc_product_keys_email', array(
      'from' => uc_store_email_from(),
      'addresses' => '[order:email]',
      'subject' => t('Product keys for order #[order:order-number]'),
      'message' => uc_get_message('product_keys_update_email_customer'),
      'format' => 2 /*filter_default_format()*/,
    ));

  $configs['uc_product_keys_update_email_customer'] = $rule;

  return $configs;
}
