<?php

/**
 * @file
 * Token hooks for the uc_product_keys module.
 */

/**
 * Implements hook_token_info().
 */
function uc_product_keys_token_info() {
  $types = array(
    'name' => t('Orders'),
    'description' => t('Tokens related to Ubercart orders.'),
    'needs-data' => 'uc_order',
  );

  $tokens = array();

  $tokens['product-keys'] = array(
    'name' => t('Product keys'),
    'description' => t('Product keys associated with an order.'),
  );

  return array(
    'types' => array('uc_order' => $types),
    'tokens' => array('uc_order' => $tokens),
  );
}

/**
 * Implements hook_tokens().
 */
function uc_product_keys_tokens($type, $tokens, $data = array(), $options = array()) {
  $replacements = array();

  if ($type == 'uc_order' && !empty($data['uc_order'])) {
    $order = $data['uc_order'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'product-keys':
          $product_keys = uc_product_keys_db_get_keys('order', $order);
          if ($product_keys) {
            $replacements[$original] = theme('uc_product_keys_token', array('keys' => $product_keys));
          }
          break;
      }
    }
  }

  return $replacements;
}
