<?php

/**
 * @file
 * This handles integration with Ubercart's rules interface.
 */

/**
 * Implements hook_rules_event_info().
 */
function uc_product_keys_rules_event_info() {
  $events['uc_product_keys_assigned'] = array(
    'label' => t('Product keys are assigned to a customer'),
    'group' => t('Product keys'),
    'variables' => array(
      'order' => array(
        'type' => 'uc_order',
        'label' => t('Updated order'),
      ),
    ),
  );

  return $events;
}

/**
 * Implements hook_rules_action_info().
 */
function uc_product_keys_rules_action_info() {
  $actions['uc_product_keys_email'] = array(
    'label' => t('E-mail product keys to a user'),
    'group' => t('Product keys'),
    'base' => 'uc_product_keys_action_email',
    'parameter' => array(
      'order' => array(
        'type' => 'uc_order',
        'label' => t('Product keys order'),
      ),
      'from' => array(
        'type' => 'text',
        'label' => t('Sender'),
        'description' => t("Enter the 'From' email address, or leave blank to use your store email address. You may use order tokens for dynamic email addresses."),
        'optional' => TRUE,
      ),
      'addresses' => array(
        'type' => 'text',
        'label' => t('Recipients'),
        'description' => t('Enter the email addresses to receive the notifications, one on each line. You may use order tokens for dynamic email addresses.'),
      ),
      'subject' => array(
        'type' => 'text',
        'label' => t('Subject'),
        'translatable' => TRUE,
      ),
      'message' => array(
        'type' => 'text',
        'label' => t('Message'),
        'translatable' => TRUE,
      ),
      'format' => array(
        'type' => 'text',
        'label' => t('Message format'),
        'options list' => 'uc_order_message_formats',
      ),
    ),
  );

  return $actions;
}

/******************************************************************************
 * Action Callbacks and Forms                                                 *
 ******************************************************************************/

/**
 * Email product keys to a customer
 *
 * The from, addresses, subject, and message fields take product key token
 * replacements.
 *
 * @param $order The order in which the keys were purchased.
 * @param $from The from email address.
 * @param $addresses List of recipient email addresses.
 * @param $subject Subject of the email.
 * @param $message Body of the email.
 * @param $format Drupal format of the email body.
 * @see uc_product_keys_rules_action_info()
 * @see uc_product_keys_action_email_form()
 */
function uc_product_keys_action_email($order, $from, $addresses, $subject, $message, $format) {
  $settings = array(
    'from' => $from,
    'addresses' => $addresses,
    'subject' => $subject,
    'message' => $message,
    'format' => $format,
  );

  // Token replacements for the subject and body
  $settings['replacements'] = array(
    'order' => $order,
  );

  // Apply token replacements to the 'from' e-mail address.
  $from = token_replace($settings['from'], $settings['replacements']);
  if (empty($from)) {
    $from = uc_store_email_from();
  }

  // Apply token replacements to 'recipient' e-mail addresses.
  $addresses = token_replace($settings['addresses'], $settings['replacements']);
  // Split up our recipient e-mail addresses.
  $recipients = array();
  foreach (explode("\n", $addresses) as $address) {
    $address = trim($address);
    // Remove blank lines
    if (!empty($address)) {
      $recipients[] = $address;
    }
  }

  foreach ($recipients as $email) {
    $sent = drupal_mail('uc_product_keys', 'action-mail', $email, uc_store_mail_recipient_language($email), $settings, $from);

    if (!$sent['result']) {
      watchdog('uc_product_keys', 'Attempt to e-mail @email concerning product keys for order @order_id failed.', array('@email' => $email, '@order_id' => $order->order_id), WATCHDOG_ERROR);
    }
  }
}
